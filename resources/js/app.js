require('./bootstrap');
import Vue from 'vue'


import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

//import routes from './routes'

//import Vuetify from 'vuetify'
//Vue.use(Vuetify)
import App from './components/App'
import Home from './components/Home'
import About from './components/About'
import Desks from './components/desks/Desks'
import showDesk from './components/desks/ShowDesk'




const router = new VueRouter({
	mode: 'history',
	routes: 
	[
			{ 
			  path: '/',
			  component: Home,
			  name: 'home'
			},
			  {
			  path: '/about',
			  component: About,
			  name: 'about'
			},
			 {
			  path: '/desks',
			  component: Desks,
			  name: 'desks'
			},
			{
			  path: '/desks/:deskId',
			  component: showDesk,
			  name: 'ShowDesk',
			  props: true
			},
			

	]
})

const app = new Vue({
    el: '#app',
    components: { App },
    router
   // router: new VueRouter(routes),
    //vuetify: new Vuetify()
})
