<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class StoreDeskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'name' => 'required|max:255|unique:desks,name'
        //    'name' => 'required|max:255|unique:desks,name,'.$this->desk->id //not work
       //  'name' => 'required|max:255|unique:desks,name',
         //       Rule::unique('desks')->ignore($this->desks->id, 'id')   
          
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'Имя доски должно быть уникальное',
        ];
    }
}
