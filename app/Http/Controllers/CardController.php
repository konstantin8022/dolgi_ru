<?php

namespace App\Http\Controllers;

use App\Models\Card;
use Illuminate\Http\Request;
use App\Http\Resources\CardResource;
use App\Http\Requests\StoreCardRequest;
use Illuminate\Http\Response;
class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  CardResource::collection(Card::with('tasks')->get());

     }   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCardRequest $request)
    {
       //dd($request->all());
        $rez=Card::create($request->validated());

        return new CardResource($rez);




    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Card  $card
     * @return \Illuminate\Http\Response
     */
    //public function show($id)
   public function show(Card $card)
    {
        //return Card::findOrFail($id);  new DeskListResource
       //return  new CardResource(Card::findOrFail($id));
        return  new CardResource($card);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCardRequest  $request, Card $card)
    {
        $card->update($request->validated());

        return  new CardResource($card); 


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        
        $card -> delete();

        return response(null, Response::HTTP_NO_CONTENT );
    }
}
