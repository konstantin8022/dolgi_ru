<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DeskController;

use App\Http\Controllers\ListController;
use App\Http\Controllers\CardController;

use App\Http\Controllers\TaskController;
/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::apiResources([
    'desks' => DeskController::class,
    'lists' => ListController::class,
    'cards' => CardController::class,
    'tasks' => TaskController::class,
]);
